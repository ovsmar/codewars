//! a function that can transform a number into a string.

function numberToString(num) {
    return num + '';
  };


  //! convert boolean values to strings 'Yes' or 'No'.
  function boolToWord( bool ){
    //...
    let a = bool.toString();
    if(a=='true')
      return 'Yes';
    else
      return 'No';
  };

  //!Find the odd int 
  const findOdd = (xs) => xs.reduce((a,b) => a ^ b );

  //!Basic Mathematical Operations
function basicOp(operation , value1, value2){

  if (operation == "+")
    return value1 + value2;

    if (operation == "-")
      return value1 - value2;

      if (operation == "*")
        return value1 * value2;

        if (operation == "/")
          return value1 / value2;
};


//! Convert number to reversed array of digits 
function digitize(num) {
  return num.toString().split('').reverse().map(Number);
  
}

//!Count the number of Duplicates
function duplicateCount(text){
  let result = 0,
      soltingObject = {};
       }